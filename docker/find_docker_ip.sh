#!/bin/bash 

ip addr | grep "inet" | grep -v "127.0.0.1" |  cut -d'/' -f 1  | tr -d [:alpha:]
#List all IP addr info | filter out lines with addresses | grab the host part of an IPv4 address (part before '/') | remove all alphabet chars on that line - "inet" etc
