#!/bin/bash
#To exit a tmux session, use Ctrl+B and then press D. 
#To enter a tmux session, use tmux at -t <session_name> where session_name is decryptor, verifier or php_server.
#List existing sessions by pressing tmux ls.

cd ~/graphene && make SGX=1 sgx-tokens
cd ~/source/Decryptor && tmux new-session -d -s decryptor './app; bash';
cd ~/graphene/LibOS/shim/test/native && tmux new-session -d -s verifier './pal_loader SGX verifier; bash'
sleep 60; #Should have better logic here to check if the previous command exited successfully (server should only be started after the verifier finishes, or else it will try to do LA with the decryptor and crash' 
cd ~/graphene/LibOS/shim/test/apps/apache && tmux new-session -d -s php_server "SGX=1 make start-graphene-server; bash"

